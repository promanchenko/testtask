﻿using System.ComponentModel.DataAnnotations;

namespace TestTask.Model
{
    public class ChangeStatusViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public Statuses Status { get; set; }
        [Required]
        [MaxLength(250)]
        public string Reason { get; set; }
    }
}
