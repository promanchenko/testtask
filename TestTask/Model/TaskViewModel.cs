﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTask.Model
{
    public class TaskViewModel
    {
        public int Id { get; set; }
        public int AccountHolderId { get; set; }
        public string AccountHolderName { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public Statuses Status { get; set; }
        public string StatusDescription { get; set; }
        public string Reason { get; set; }
    }
}
