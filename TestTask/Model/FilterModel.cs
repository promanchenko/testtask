﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestTask.Model
{
    public class FilterModel
    {
        [Required]
        public int PageNo { get; set; }
        [Required]
        public int PageSize { get; set; }
        [Required]
        public List<int> Statuses { get; set; }
    }
}
