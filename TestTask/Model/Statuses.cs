﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTask.Model
{
    public enum Statuses
    {
        Pending = 0,
        Approved = 1,
        Rejected = 99
    }
}
