﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using TestTask.Model;

namespace TestTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private const string CacheKey = "_Data";
        private IMemoryCache _cache;

        public ValuesController(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            List<TaskViewModel> tasks;
            if (!_cache.TryGetValue(CacheKey, out tasks))
            {
                tasks = new List<TaskViewModel>()
                {
                    new TaskViewModel
                    {
                        Id = 832321,
                        AccountHolderId = 15651,
                        AccountHolderName = "Alex Dumsky",
                        PaymentDate = DateTime.Parse("2015-01-23T18:25:43.511Z"),
                        Amount = (decimal) 445.12,
                        Currency = "EUR",
                        Status = Statuses.Pending,
                        StatusDescription = "Pending",
                        Reason = string.Empty
                    },
                    new TaskViewModel
                    {
                        Id = 806532,
                        AccountHolderId = 46556,
                        AccountHolderName = "Dudi Elias",
                        PaymentDate = DateTime.Parse("2015-02-10T18:25:43.511Z"),
                        Amount = (decimal) 4511.12,
                        Currency = "EUR",
                        Status = Statuses.Pending,
                        StatusDescription = "Pending",
                        Reason = string.Empty
                    },
                    new TaskViewModel
                    {
                        Id = 7845431,
                        AccountHolderId = 48481,
                        AccountHolderName = "Niv Cohen",
                        PaymentDate = DateTime.Parse("2015-04-01T18:25:43.511Z"),
                        Amount = (decimal) 10.99,
                        Currency = "USD",
                        Status = Statuses.Approved,
                        StatusDescription = "Approved",
                        Reason = "Good Person"
                    },
                    new TaskViewModel
                    {
                        Id = 545341,
                        AccountHolderId = 32131,
                        AccountHolderName = "Alex Dumsky",
                        PaymentDate = DateTime.Parse("2016-02-21T18:25:43.511Z"),
                        Amount = (decimal) 9952.48,
                        Currency = "EUR",
                        Status = Statuses.Approved,
                        StatusDescription = "Rejected",
                        Reason = "This is suspicious. it's a too long text"
                    }
                };

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromSeconds(600));

                _cache.Set(CacheKey, tasks, cacheEntryOptions);
            }
        }

        [HttpGet]
        public string Get()
        {
            return "works";
        }

        [HttpGet]
        [Route("list")]
        public ActionResult<IEnumerable<TaskViewModel>> Get([FromQuery]FilterModel model)
        {
            if (!ModelState.IsValid)
            {
                return null;
            }

            if (model.Statuses == null)
            {
                return null;
                // throw new ArgumentNullException(nameof(model.Statuses));
            }
            var tasks = _cache.Get<List<TaskViewModel>>(CacheKey);
            
            return tasks.Where(t => model.Statuses.Contains((int) t.Status)).ToList();
        }


        [HttpPost]
        [Route("changeStatus")]
        public IActionResult Post([FromBody] ChangeStatusViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tasks = _cache.Get<List<TaskViewModel>>(CacheKey);
            var task = tasks.FirstOrDefault(t => t.Id == model.Id);

            if (task == null)
            {
                return BadRequest("The task is not found");
            }

            task.Status = model.Status;
            task.Reason = model.Reason;

            return Ok();
        }
    }
}
