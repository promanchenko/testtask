import {Injectable} from '@angular/core';
import {BASE_URL} from './constants/constants';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TaskViewModel, ChangeStatusViewModel} from './task-view-model.model';

@Injectable()
export class DataService {
  private url = `${BASE_URL}api/values`;

  constructor(private http: HttpClient) {
  }

  getList(data): Observable<any> {
    return this.http.get<TaskViewModel[]>(`${this.url}/list`, {params : data});
  }

  changeStatus(model: ChangeStatusViewModel) {
    return this.http.post(`${this.url}/changeStatus`, model);
  }
}
