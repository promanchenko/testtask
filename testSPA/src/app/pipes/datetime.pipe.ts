import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import {DATE_TIME_FMT} from '../constants/constants';

@Pipe({
  name: 'dateTimeFormat'
})

export class DateTimeFormatPipe  extends DatePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    const dateValue = new Date(value);

    const dateWithNoTimezone = new Date(
      dateValue.getUTCFullYear(),
      dateValue.getUTCMonth(),
      dateValue.getUTCDate(),
      dateValue.getUTCHours(),
      dateValue.getUTCMinutes(),
      dateValue.getUTCSeconds()
    );
    return super.transform(dateWithNoTimezone, DATE_TIME_FMT);
  }
}
