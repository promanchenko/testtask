import { environment } from '../../environments/environment';

export const BASE_URL = environment.apiUrl;
export const DATE_FMT = 'dd/MM/yyyy';
export const DATE_TIME_FMT = `${DATE_FMT} HH:MM`;
