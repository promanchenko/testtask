export enum Statuses {
  Pending = 0,
  Approved = 1,
  Rejected = 99
}

export class TaskViewModel {
  id: number;
  accountHolderId: number;
  accountHolderName: string;
  paymentDate: Date;
  amount: number;
  currency: string;
  status: Statuses;
  statusDescription: string;
  reason: string;
}

export interface IPage {
  pageNo: number;
  pageSize: number;
}

export interface IFilter  extends IPage {
  statuses: number[];
}

export class ChangeStatusViewModel {
  id: number;
  status: Statuses;
  reason: string;
}

