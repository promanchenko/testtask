import {Component, OnInit, ViewChild} from '@angular/core';
import {MatButtonToggleGroup, MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {ChangeStatusDialogComponent} from '../change-status-dialog/change-status-dialog.component';
import {DataService} from '../../data.service';
import {ChangeStatusViewModel, IFilter, Statuses} from '../../task-view-model.model';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public isLoading: boolean;
  public statuses = Statuses;
  public displayedColumns = ['paymentDate', 'status', 'amount', 'edit', 'reason'];
  public dataSource;
  public filter: number[] = [Statuses.Pending];
  public routeParams: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatButtonToggleGroup) group: MatButtonToggleGroup;

  constructor(public dialog: MatDialog,
              private router: Router,
              private dataService: DataService) {
    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        let leaf = this.router.routerState.snapshot.root;
        while (leaf && leaf.children && leaf.children.length > 0 && Object.keys(leaf.params).length === 0) {
          leaf = leaf.children[0];
        }
        this.routeParams = leaf.params;
      }
    });
  }

  ngOnInit(): void {
    this.refreshList();
    if (this.routeParams && this.routeParams.id) {
      setTimeout(() => this.changeStatus(this.routeParams.id));
    }
  }

  refreshList() {
    this.isLoading = true;
    const filter = this.getFilter();
    this.dataService.getList(filter).subscribe(
      data => {
        this.isLoading = false;
        if (!data) {
          return;
        }
        this.dataSource = new MatTableDataSource(data);
      },
      () => this.isLoading = false
    );
  }

  applyFilter(statuses) {
    this.filter = statuses;
    this.refreshList();
  }

  changeStatus(id) {
    const dialogRef = this.dialog.open(ChangeStatusDialogComponent, {
      width: '500px'
    });
    dialogRef.componentInstance.id = id;
    const sub = dialogRef.componentInstance.handleChangeStatus.subscribe((result: ChangeStatusViewModel) => {
      if (result) {
        dialogRef.close();
        this.dataService.changeStatus(result).subscribe(resp => {
            this.refreshList();
        });
      }
    });
    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  private getFilter(): IFilter {
    return {
      pageNo: 1, //this.pageIndex + 1,
      pageSize: 5,
      statuses: this.filter
    };
  }

}
