import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

import { ListComponent } from './list.component';
import {MaterialModule} from '../../modules/material.module';
import {DateTimeFormatPipe} from '../../pipes/datetime.pipe';
import {DateFormatPipe} from '../../pipes/date.pipe';
import {RouterTestingModule} from '@angular/router/testing';
import {DataService} from '../../data.service';
import {HttpClientModule} from '@angular/common/http';
import {MockBackend} from '@angular/http/testing';
import {BaseRequestOptions, Jsonp, ResponseOptions} from '@angular/http';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;
  let dataService: DataService;
  let httpMock: HttpTestingController;
  let backend: MockBackend;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListComponent,
        DateFormatPipe,
        DateTimeFormatPipe
      ],
      imports: [
        HttpClientModule,
        RouterTestingModule,
        MaterialModule,
        HttpClientTestingModule
      ],
      providers: [
        DataService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Jsonp,
          useFactory: (back, options) => new Jsonp(back, options),
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    }).compileComponents();

    dataService = TestBed.get(DataService);
    backend = TestBed.get(MockBackend);
    httpMock = TestBed.get(HttpTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get data', () => {

    expect(component).toBeTruthy();
  });

  it('should return a task', fakeAsync(() => {
    const response = {
      data: [
        {
          'id': 832321,
          'accountHolderId': '15651',
          'accountHolderName': 'Alex Dumsky',
          'paymentDate': '2015-01-23T18:25:43.511Z',
          'amount': '445.12',
          'currency': 'EUR',
          'status': 0,
          'statusDescription': 'Pending',
          'reason': null
        }
      ]
    };

    backend.connections.subscribe((connection) => {
      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(response)
      })));
    });

    dataService.getList({}).subscribe((data) => {
      expect(data.length).toBe(1);
      expect(data[0].id).toBe(832321);
      expect(data[0].status).toBe(0);
      expect(data[0].statusDescription).toBe('Pending');
    });

  }));

});
