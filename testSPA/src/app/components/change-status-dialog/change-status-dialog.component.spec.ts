import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeStatusDialogComponent } from './change-status-dialog.component';
import {MaterialModule} from '../../modules/material.module';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {MatButton} from '@angular/material';

describe('ChangeStatusDialogComponent', () => {
  let component: ChangeStatusDialogComponent;
  let fixture: ComponentFixture<ChangeStatusDialogComponent>;
  let submitEl: DebugElement;
  let changeStatusForm;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeStatusDialogComponent ],
      imports: [
        BrowserAnimationsModule,
        MaterialModule,
        ReactiveFormsModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeStatusDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    changeStatusForm = component.changeStatusForm;
    submitEl = fixture.debugElement.query(By.directive(MatButton));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('reason field required validation ', () => {
    const reason = changeStatusForm.controls['reason'];
    expect(reason).not.toBeNull();
    submitEl.triggerEventHandler('click', null);
    expect(reason.hasError('required')).toBeTruthy();
  });

  it('reason field max length validation', () => {
    const reason = changeStatusForm.controls['reason'];
    expect(reason).not.toBeNull();

    submitEl.triggerEventHandler('click', null);
    reason.setValue('test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test testtest test test test test test test testtest test test test test test test test');
    expect(reason.hasError('maxlength')).toBeTruthy();
  });

  it('should update model before on submit', () => {
    expect(changeStatusForm.valid).toBeFalsy();

    changeStatusForm.controls['status'].setValue(1);
    changeStatusForm.controls['reason'].setValue('test');

    expect(changeStatusForm.valid).toBeTruthy();
  });

});
