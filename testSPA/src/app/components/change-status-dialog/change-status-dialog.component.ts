import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ChangeStatusViewModel, Statuses} from '../../task-view-model.model';

@Component({
  selector: 'app-change-status-dialog',
  templateUrl: './change-status-dialog.component.html',
  styleUrls: ['./change-status-dialog.component.css']
})
export class ChangeStatusDialogComponent implements OnInit {
  public id: number;
  public statuses = Statuses;

  public changeStatusForm = this.fb.group({
    status: [Statuses.Approved, [Validators.required]],
    reason: ['', [Validators.required, Validators.maxLength(250)]]
  });

  @Output() public handleChangeStatus: EventEmitter<Object> = new EventEmitter();
  constructor(public fb: FormBuilder) { }

  ngOnInit() {
  }

  changeStatus() {
    if (this.changeStatusForm.valid) {
      const model: ChangeStatusViewModel = this.changeStatusForm.value;
      model.id = this.id;
      this.handleChangeStatus.emit(model);
    }
  }

}
