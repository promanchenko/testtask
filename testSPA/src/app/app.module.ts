import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {DataService} from './data.service';
import {MaterialModule} from './modules/material.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DateTimeFormatPipe} from './pipes/datetime.pipe';
import {DateFormatPipe} from './pipes/date.pipe';
import { ChangeStatusDialogComponent } from './components/change-status-dialog/change-status-dialog.component';
import { ListComponent } from './components/list/list.component';
import {AppRoutingModule} from './app-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {ErrorsHttpInterceptor} from './errors-http.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    DateFormatPipe,
    DateTimeFormatPipe,
    ChangeStatusDialogComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    MaterialModule
  ],
  entryComponents: [
    ChangeStatusDialogComponent
  ],
  providers: [
    DataService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorsHttpInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
